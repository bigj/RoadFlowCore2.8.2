﻿//日期
; RoadUI.Calendar = function () {
    var instance = this;
    this.init = function ($cts) {
        $.each($cts, function () {
            var istime = $(this).attr("istime") || "";
            var range = $(this).attr("range") || "";
            var minDate = $(this).attr("mindate") || "";
            var maxDate = $(this).attr("maxdate") || "";
            var datetype = $(this).attr("datetype") || "";
            var daybefor = $(this).attr("daybefor") || "";
            var dayafter = $(this).attr("dayafter") || "";
            var currentmonth = $(this).attr("currentmonth") || "";
            var format = $(this).attr("format") || "";
            if (!format || format.length == 0) {
                format = "yyyy-MM-dd";
                if (istime && (istime.toLowerCase() == "true" || istime == "1")) {
                    format = "yyyy-MM-dd HH:mm:ss";
                }
            }
            if (!datetype) {
                datetype = istime && (istime.toLowerCase() == "true" || istime == "1") ? "datetime" : "date";
            }
            var opts = { elem: $(this).get(0), format: format, type: datetype };
            if (minDate) {
                opts.min = minDate;
            }
            if (maxDate) {
                opts.max = maxDate;
            }
            if (range && range.toLowerCase() == "true" || range == "1")//是否是选择范围
            {
                opts.range = range;
            }

            //今天之前
            if (daybefor && (daybefor.toLowerCase() == "true" || daybefor == "1")) {
                opts.max = new Date().toString();
            }
            //今天之后
            if (dayafter && (dayafter.toLowerCase() == "true" || dayafter == "1")) {
                opts.min = new Date().toString();
            }
            //当前月
            if (currentmonth && (currentmonth.toLowerCase() == "true" || currentmonth == "1")) {
                currentDate = new Date();
                currentYear = currentDate.getFullYear();
                currentMonth1 = currentDate.getMonth() + 1;
                currentDay = currentDate.getDate();
                if (dayafter && (dayafter.toLowerCase() == "true" || dayafter == "1")) {
                    opts.min = currentYear + "-" + currentMonth1 + "-" + currentDay;
                } else {
                    opts.min = currentYear + "-" + currentMonth1 + '-01';
                }
                if (daybefor && (daybefor.toLowerCase() == "true" || daybefor == "1")) {
                    opts.max = currentYear + "-" + currentMonth1 + "-" + currentDay;
                } else {
                    opts.max = currentYear + "-" + currentMonth1 + '-' + laydate.getEndDate(currentYear, currentMonth1);
                }
            }
            $(this).removeClass().addClass("mytext");
            laydate.render(opts);
        });
    };
}