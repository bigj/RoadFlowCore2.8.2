﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RoadFlow.Utility;
using Newtonsoft.Json.Linq;

namespace RoadFlow.Mvc.Areas.RoadFlowCore.Controllers
{
    [Area("RoadFlowCore")]
    public class OnlineUserController : Controller
    {
        [Validate]
        public IActionResult Index()
        {
            var ous = Business.OnlineUser.GetAll();
            JArray jArray = new JArray();
            foreach (var ou in ous)
            {
                JObject jObject = new JObject
                {
                    { "id", ou.UserId.ToString() },
                    { "Name", ou.UserName },
                    { "Organize", ou.UserOrganize },
                    { "LoginTime", ou.LoginTime.ToDateTimeString() },
                    { "LastTime", ou.LastTime.ToDateTimeString() },
                    { "LastUrl", ou.LastUrl },
                    { "IP", ou.IP },
                    { "Agent", ou.BrowseAgent }
                };
                jArray.Add(jObject);
            }
            ViewData["json"] = jArray.ToString(Newtonsoft.Json.Formatting.None);
            ViewData["appId"] = Request.Querys("appid");
            ViewData["tabId"] = Request.Querys("tabid");
            ViewData["queryString"] = Request.UrlQuery();
            return View();
        }

        [Validate]
        [ValidateAntiForgeryToken]
        public string Clear()
        {
            string[] ids = Request.Forms("ids").Split(',');
            foreach (string id in ids)
            {
                if (!id.IsGuid(out Guid userId))
                {
                    continue;
                }
                Business.OnlineUser.Remove(userId);
            }
            return "清除成功!";
        }
    }
}