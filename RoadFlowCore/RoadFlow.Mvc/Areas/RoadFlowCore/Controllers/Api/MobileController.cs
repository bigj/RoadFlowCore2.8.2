﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using RoadFlow.Utility;

namespace RoadFlow.Mvc.Areas.RoadFlowCore.Controllers.Api
{
    [Route("RoadFlowCore/Api/[controller]")]
    [ApiController]
    public class MobileController : ControllerBase
    {
        /// <summary>
        /// 查询待办事项
        /// </summary>
        /// <returns></returns>
        [HttpPost, Route("QueryWaitTask")]
        public string QueryWaitTask()
        {
            Guid userId = Current.EnterpriseWeiXinUserId;
            int pageNumber = Request.Forms("pagenumber").ToInt(1);
            string title = Request.Forms("title");
            var waitTasks = new Business.FlowTask().GetWaitTask(8, pageNumber, userId, "", title, "", "", "ReceiveTime DESC", out int waitTaskCount);
            JArray jArray = new JArray();
            foreach (DataRow dr in waitTasks.Rows)
            {
                JObject jObject = new JObject
                {
                    { "Id", dr["Id"].ToString() },
                    { "FlowId", dr["FlowId"].ToString() },
                    { "StepId", dr["StepId"].ToString() },
                    { "InstanceId", dr["InstanceId"].ToString() },
                    { "GroupId", dr["GroupId"].ToString() },
                    { "Title", dr["Title"].ToString() },
                    { "SenderName", dr["SenderName"].ToString() },
                    { "ReceiveTime", dr["ReceiveTime"].ToString().ToDateTime().ToShortDateTimeString() }
                };
                jArray.Add(jObject);
            }
            JObject o = new JObject
            {
                { "total", waitTaskCount },
                { "hasnext", 8 * pageNumber >= waitTaskCount ? 0 : 1},//是否还有下一页
                { "data", jArray }
            };
            return o.ToString(Newtonsoft.Json.Formatting.None);
        }

        /// <summary>
        /// 查询已办事项
        /// </summary>
        /// <returns></returns>
        [HttpPost, Route("QueryCompletedTask")]
        public string QueryCompletedTask()
        {
            Guid userId = Current.EnterpriseWeiXinUserId;
            int pageNumber = Request.Forms("pagenumber").ToInt(1);
            string title = Request.Forms("title");
            var waitTasks = new Business.FlowTask().GetCompletedTask(8, pageNumber, userId, "", title, "", "", "ReceiveTime DESC", out int waitTaskCount);
            JArray jArray = new JArray();
            foreach (DataRow dr in waitTasks.Rows)
            {
                JObject jObject = new JObject
                {
                    { "Id", dr["Id"].ToString() },
                    { "FlowId", dr["FlowId"].ToString() },
                    { "StepId", dr["StepId"].ToString() },
                    { "InstanceId", dr["InstanceId"].ToString() },
                    { "GroupId", dr["GroupId"].ToString() },
                    { "Title", dr["Title"].ToString() },
                    { "SenderName", dr["SenderName"].ToString() },
                    { "CompletedTime1", dr["CompletedTime1"].ToString().ToDateTime().ToShortDateTimeString() }
                };
                jArray.Add(jObject);
            }
            JObject o = new JObject
            {
                { "total", waitTaskCount },
                { "hasnext", 8 * pageNumber >= waitTaskCount ? 0 : 1},//是否还有下一页
                { "data", jArray }
            };
            return o.ToString(Newtonsoft.Json.Formatting.None);
        }

        /// <summary>
        /// 查询可以发起的流程
        /// </summary>
        /// <returns></returns>
        [HttpPost, Route("QueryStatrFlow")]
        public string QueryStatrFlow()
        {
            Guid userId = Current.EnterpriseWeiXinUserId;
            var flows = new Business.Flow().GetStartFlows(userId);
            JArray jArray = new JArray();
            var dict = new Business.Dictionary();
            string rootId = dict.GetIdByCode("system_applibrarytype").ToString();
            foreach (var flow in flows.GroupBy(p => p.Type).OrderBy(p => p.Key))
            {
                string type = dict.GetAllParentTitle(flow.Key, true, false, rootId, false);
                JObject jObject1 = new JObject
                {
                    { "type", type }
                };
                JArray jArray1 = new JArray();
                foreach (var f in flow)
                {
                    string bgcolor = f.Color.IsNullOrWhiteSpace() ? "#117eef" : f.Color;
                    string ico = f.Ico.IsNullOrEmpty() ? "fa-pencil-square-o" : f.Ico;
                    JObject jObject = new JObject
                    {
                        { "id", f.Id },
                        { "name", f.Name },
                        { "ico", ico },
                        { "bgcolor", bgcolor }
                    };
                    jArray1.Add(jObject);
                }
                jObject1.Add("flows", jArray1);
                jArray.Add(jObject1);
            }
            return jArray.ToString(Newtonsoft.Json.Formatting.None);
        }

        /// <summary>
        /// 查询一页文档
        /// </summary>
        /// <returns></returns>
        [HttpPost, Route("QueryDocument")]
        public string QueryDocument()
        {
            Guid userId = Current.EnterpriseWeiXinUserId;
            string dirId = Request.Forms("dirid");
            int pageNumber = Request.Forms("pagenumber").ToInt(1);
            string title = Request.Forms("title");
            string order = "Rank,WriteTime DESC";
            string dir = dirId.IsGuid() ? "'" + dirId + "'" : "";

            Business.Doc doc = new Business.Doc();
            DataTable dt = doc.GetPagerList(out int count, 8, pageNumber, userId, title, dir, "", "", order, -1);
            JArray jArray = new JArray();
            foreach (DataRow dr in dt.Rows)
            {
                JObject jObject = new JObject
                {
                    { "id", dr["Id"].ToString() },
                    { "title", dr["Title"].ToString() },
                    { "userName", dr["WriteUserName"].ToString() },
                    { "writeTime", dr["WriteTime"].ToString().ToDateTime().ToShortDateTimeString() }
                };
                jArray.Add(jObject);
            }
            JObject json = new JObject
            {
                { "name", new Business.DocDir().GetAllParentNames(dirId.ToGuid(), true, false) },
                { "total", count },
                { "hasnext", 8 * pageNumber >= count ? 0 : 1},//是否还有下一页
                { "data", jArray }
            };
            return json.ToString(Newtonsoft.Json.Formatting.None);
        }
    }
}