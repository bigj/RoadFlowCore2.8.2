﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using RoadFlow.Utility;
using Microsoft.AspNetCore.Hosting;

namespace RoadFlow.IntegrateCore
{
    public class Current
    {
        /// <summary>
        /// 当前http请求
        /// </summary>
        public static HttpContext HttpContext
        {
            get
            {
                return Tools.HttpContext;
            }
        }

        /// <summary>
        /// 当前登录用户ID
        /// </summary>
        public static Guid UserId
        {
            get
            {
                return Business.User.CurrentUserId;
            }
        }

        /// <summary>
        /// 当前登录用户实体
        /// </summary>
        public static Model.User User
        {
            get
            {
                return Business.User.CurrentUser;
            }
        }

        /// <summary>
        /// 当前登录用户姓名
        /// </summary>
        public static string UserName
        {
            get
            {
                return Business.User.CurrentUserName;
            }
        }

        /// <summary>
        /// web目录绝对路径(包含wwwroot)
        /// </summary>
        public static string WebRootPath
        {
            get
            {
                return Tools.GetWebRootPath();
            }
        }

        /// <summary>
        /// 得到站点目录绝对路径
        /// </summary>
        public static string ContentRootPath
        {
            get
            {
                return Tools.GetContentRootPath();
            }
        }

        /// <summary>
        /// 当前日期时间
        /// </summary>
        public static DateTime DateTime
        {
            get
            {
                return DateExtensions.Now;
            }
        }
    }
}
