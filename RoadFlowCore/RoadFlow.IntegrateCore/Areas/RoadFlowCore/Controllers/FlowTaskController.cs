﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RoadFlow.Utility;
using System.Data;

namespace RoadFlow.IntegrateCore.Areas.RoadFlowCore.Controllers
{
    [Area("RoadFlowCore")]
    public class FlowTaskController : Controller
    {
        /// <summary>
        /// 待办事项
        /// </summary>
        /// <returns></returns>
        [Validate(CheckApp = false)]
        public IActionResult Wait()
        {
            ViewData["flowOptions"] = new Business.Flow().GetOptions();
            ViewData["query"] = "appid=" + Request.Querys("appid") + "&tabid=" + Request.Querys("tabid");
            return View();
        }
        /// <summary>
        /// 查询待办事项
        /// </summary>
        /// <returns></returns>
        [Validate(CheckApp = false)]
        public string QueryWait()
        {
            string appid = Request.Querys("appid");
            string tabid = Request.Querys("tabid");
            string flowid = Request.Forms("flowid");
            string title = Request.Forms("title");
            string date1 = Request.Forms("date1");
            string date2 = Request.Forms("date2");
            string sidx = Request.Forms("sidx");
            string sord = Request.Forms("sord");
            string order = (sidx.IsNullOrEmpty() ? "ReceiveTime" : sidx) + " " + (sord.IsNullOrEmpty() ? "ASC" : sord);
            int size = Tools.GetPageSize();
            int number = Tools.GetPageNumber();

            DataTable dataTable = new Business.FlowTask().GetWaitTask(size, number, Current.UserId, flowid, title, date1, date2, order, out int count);
            Newtonsoft.Json.Linq.JArray jArray = new Newtonsoft.Json.Linq.JArray();
            foreach (DataRow dr in dataTable.Rows)
            {
                int openModel = 0, width = 0, height = 0;
                string opation = "<a href=\"javascript:void(0);\" class=\"list\" onclick=\"openTask('" + Url.Content("~/RoadFlowCore/FlowRun/Index") + "?" + string.Format("flowid={0}&stepid={1}&instanceid={2}&taskid={3}&groupid={4}&appid={5}",
                        dr["FlowId"], dr["StepId"], dr["InstanceId"], dr["Id"], dr["GroupId"], appid
                        ) + "','" + dr["Title"].ToString().RemoveHTML().UrlEncode() + "','" + dr["Id"] + "'," + openModel + "," + width + "," + height + ");return false;\"><i class=\"fa fa-pencil-square-o\"></i>处理</a>"
                        + "<a class=\"list\" href=\"javascript:void(0);\" onclick=\"detail('" + dr["FlowId"] + "','" + dr["GroupId"] + "','" + dr["Id"] + "');return false;\"><i class=\"fa fa-search\"></i>查看</a>";
                //if (flowRunModel != null && flowRunModel.FirstStepID == task.StepID && task.SenderID == userID)//第一步发起者可以删除
                //{
                //    opation += "&nbsp;&nbsp;<a class=\"deletelink\" href=\"javascript:void(0);\" onclick=\"delTask('" + dr["FlowId"] + "','" + dr["GroupId"] + "','" + dr["Id"] + "');return false;\">作废</a>";
                //}
                string taskTitle = "<a href=\"javascript:void(0);\" class=\"list\" onclick=\"openTask('" + Url.Content("~/RoadFlowCore/FlowRun/Index") + "?" + string.Format("flowid={0}&stepid={1}&instanceid={2}&taskid={3}&groupid={4}&appid={5}",
                        dr["FlowId"], dr["StepId"], dr["InstanceId"], dr["Id"], dr["GroupId"], appid
                        ) + "','" + dr["Title"].ToString().RemoveHTML().UrlEncode() + "','" + dr["Id"] + "'," + openModel + "," + width + "," + height + ");return false;\">" + dr["Title"] + "</a>";
                string status = "<div>正常</div>";
                if (dr["CompletedTime"].ToString().IsDateTime(out DateTime ctime))
                {
                    DateTime nowTime = DateExtensions.Now;
                    if (nowTime > ctime)
                    {
                        status = "<div title='要求完成时间：" + ctime.ToString("yyyy-MM-dd HH:mm") +
                            "' style='color:red;font-weight:bold;'>已超期</div>";
                    }
                    else if ((ctime - nowTime).Days < 3)
                    {
                        double day = Math.Ceiling((ctime - nowTime).TotalDays);
                        string dsystring = day == 0 ? (ctime - nowTime).Hours + "小时" : day.ToString() + "天";
                        status = "<div title='要求完成时间：" + ctime.ToString("yyyy-MM-dd HH:mm") +
                            "' style='color:#ff7302;font-weight:bold;'>" + dsystring + "内到期</div>";
                    }
                    else
                    {
                        status = "<div title='要求完成时间：" + ctime.ToString("yyyy-MM-dd HH:mm") +
                            "' style=''>正常</div>";
                    }
                }
                Newtonsoft.Json.Linq.JObject jObject = new Newtonsoft.Json.Linq.JObject
                {
                    { "id", dr["Id"].ToString() },
                    { "Title", taskTitle },
                    { "FlowName", dr["FlowName"].ToString() },
                    { "StepName", dr["StepName"].ToString() },
                    { "SenderName", dr["SenderName"].ToString() },
                    { "ReceiveTime", dr["ReceiveTime"].ToString().ToDateTime().ToDateTimeString() },
                    { "StatusTitle", status },
                    { "Note", dr["Note"].ToString() },
                    { "Opation", opation }
                };
                jArray.Add(jObject);
            }
            return "{\"userdata\":{\"total\":" + count + ",\"pagesize\":" + size + ",\"pagenumber\":" + number + "},\"rows\":" + jArray.ToString() + "}";
        }

        /// <summary>
        /// 已办事项
        /// </summary>
        /// <returns></returns>
        [Validate(CheckApp = false)]
        public IActionResult Completed()
        {
            ViewData["flowOptions"] = new Business.Flow().GetOptions();
            ViewData["query"] = "appid=" + Request.Querys("appid") + "&tabid=" + Request.Querys("tabid");
            return View();
        }

        /// <summary>
        /// 查询已办事项
        /// </summary>
        /// <returns></returns>
        [Validate(CheckApp = false)]
        public string QueryCompleted()
        {
            string appid = Request.Querys("appid");
            string tabid = Request.Querys("tabid");
            string flowid = Request.Forms("flowid");
            string title = Request.Forms("title");
            string date1 = Request.Forms("date1");
            string date2 = Request.Forms("date2");
            string sidx = Request.Forms("sidx");
            string sord = Request.Forms("sord");
            string order = (sidx.IsNullOrEmpty() ? "CompletedTime1" : sidx) + " " + (sord.IsNullOrEmpty() ? "ASC" : sord);
            int size = Tools.GetPageSize();
            int number = Tools.GetPageNumber();

            Business.FlowTask flowTask = new Business.FlowTask();
            DataTable dataTable = flowTask.GetCompletedTask(size, number, Current.UserId, flowid, title, date1, date2, order, out int count);
            Newtonsoft.Json.Linq.JArray jArray = new Newtonsoft.Json.Linq.JArray();
            foreach (DataRow dr in dataTable.Rows)
            {
                int openModel = 0, width = 0, height = 0;
                string opation =
                    "<a href=\"javascript:void(0);\" class=\"list\" onclick=\"openTask('" + Url.Content("~/RoadFlowCore/FlowRun/Index") + "?" + string.Format("flowid={0}&stepid={1}&instanceid={2}&taskid={3}&groupid={4}&appid={5}&display=1",
                        dr["FlowId"], dr["StepId"], dr["InstanceId"], dr["Id"], dr["GroupId"], appid
                        ) + "','" + dr["Title"].ToString().RemoveHTML().UrlEncode() + "','" + dr["Id"] + "'," + openModel + "," + width + "," + height + ");return false;\"><i class=\"fa fa-file-text-o\"></i>表单</a>"+
                        "<a class=\"list\" href=\"javascript:void(0);\" onclick=\"detail('" + dr["FlowId"] + "','" + dr["GroupId"] + "','" + dr["Id"] + "');return false;\"><i class=\"fa fa-navicon\"></i>处理过程</a>";
                string taskTitle = "<a href=\"javascript:void(0);\" class=\"list\" onclick=\"openTask('" + Url.Content("~/RoadFlowCore/FlowRun/Index") + "?" + string.Format("flowid={0}&stepid={1}&instanceid={2}&taskid={3}&groupid={4}&appid={5}&display=1",
                        dr["FlowId"], dr["StepId"], dr["InstanceId"], dr["Id"], dr["GroupId"], appid
                        ) + "','" + dr["Title"].ToString().RemoveHTML().UrlEncode() + "','" + dr["Id"] + "'," + openModel + "," + width + "," + height + ");return false;\">" + dr["Title"] + "</a>";
                string status = flowTask.GetExecuteTypeTitle(dr["ExecuteType"].ToString().ToInt());
                string note = dr["Note"].ToString();
                Newtonsoft.Json.Linq.JObject jObject = new Newtonsoft.Json.Linq.JObject
                {
                    { "id", dr["Id"].ToString() },
                    { "Title", taskTitle },
                    { "FlowName", dr["FlowName"].ToString() },
                    { "StepName", dr["StepName"].ToString() },
                    { "SenderName", dr["SenderName"].ToString() },
                    { "ReceiveTime", dr["ReceiveTime"].ToString() },
                    { "CompletedTime", dr["CompletedTime1"].ToString().ToDateTime().ToDateTimeString() },
                    { "Note", note },
                    { "Opation", opation }
                };
                jArray.Add(jObject);
            }
            return "{\"userdata\":{\"total\":" + count + ",\"pagesize\":" + size + ",\"pagenumber\":" + number + "},\"rows\":" + jArray.ToString() + "}";
        }

        /// <summary>
        /// 实例管理列表
        /// </summary>
        /// <returns></returns>
        [Validate]
        public IActionResult Instance()
        {
            ViewData["appid"] = Request.Querys("appid");
            ViewData["query"] = "&appid=" + Request.Querys("appid") + "&tabid=" + Request.Querys("tabid");
            ViewData["flowOptions"] = new Business.Flow().GetManageInstanceOptions(Current.UserId);
            return View();
        }

        /// <summary>
        /// 查询实例列表
        /// </summary>
        /// <returns></returns>
        [Validate]
        public string QueryInstance()
        {
            string appid = Request.Querys("appid");
            string tabid = Request.Querys("tabid");
            string FlowID = Request.Forms("FlowID");
            string Title = Request.Forms("Title");
            string ReceiveID = Request.Forms("ReceiveID");
            string Date1 = Request.Forms("Date1");
            string Date2 = Request.Forms("Date2");
            string sidx = Request.Forms("sidx");
            string sord = Request.Forms("sord");
            string order = (sidx.IsNullOrEmpty() ? "ReceiveTime" : sidx) + " " + (sord.IsNullOrEmpty() ? "DESC" : sord);
            int size = Tools.GetPageSize();
            int number = Tools.GetPageNumber();
            Business.FlowTask flowTask = new Business.FlowTask();
            Guid userId = new Business.User().GetUserId(ReceiveID);
            if (FlowID.IsNullOrWhiteSpace())
            {
                FlowID = new Business.Flow().GetManageInstanceFlowIds(Current.UserId).JoinSqlIn();
            }
            DataTable dataTable = flowTask.GetInstanceList(size, number, FlowID, Title, userId.IsEmptyGuid() ? "" : userId.ToString(), Date1, Date2, order, out int count);
            Newtonsoft.Json.Linq.JArray jArray = new Newtonsoft.Json.Linq.JArray();
            foreach (DataRow dr in dataTable.Rows)
            {
                var groupTasks = flowTask.GetListByGroupId(dr["GroupId"].ToString().ToGuid());
                Model.FlowTask taskModel = null;
                if (!Title.IsNullOrWhiteSpace())
                {
                    taskModel = groupTasks.Where(p=>p.Title.ContainsIgnoreCase(Title)).OrderByDescending(p => p.Sort).ThenBy(p => p.Status).First();
                }
                if (null == taskModel)
                {
                    taskModel = groupTasks.OrderByDescending(p => p.Sort).ThenBy(p => p.Status).First();
                }
                int openModel = 0, width = 0, height = 0;
                string opation = "<a class=\"list\" href=\"javascript:void(0);\" onclick=\"manage('" + taskModel.Id.ToString() + "', '" + taskModel.GroupId.ToString() + "');\"><i class=\"fa fa-check-square-o\"></i>管理</a>";
                //if (taskModel.ExecuteType.In(-1, 0, 1))
                //{
                    opation += "<a class=\"list\" href=\"javascript:void(0);\" onclick=\"delete1('" + taskModel.Id.ToString() + "', '" + taskModel.GroupId.ToString() + "');\"><i class=\"fa fa-trash-o\"></i>删除</a>";
                //}
               
                string status = flowTask.GetExecuteTypeTitle(taskModel.ExecuteType);
                string taskTitle = "<a href=\"javascript:void(0);\" class=\"list\" onclick=\"openTask('" + Url.Content("~/RoadFlowCore/FlowRun/Index") + "?" + string.Format("flowid={0}&stepid={1}&instanceid={2}&taskid={3}&groupid={4}&appid={5}&display=1",
                        taskModel.FlowId.ToString(), taskModel.StepId.ToString(), taskModel.InstanceId, taskModel.Id.ToString(), taskModel.GroupId.ToString(), appid
                        ) + "','" + taskModel.Title.TrimAll() + "','" + taskModel.Id.ToString() + "'," + openModel + "," + width + "," + height + ");return false;\">" + taskModel.Title + "</a>";
                Newtonsoft.Json.Linq.JObject jObject = new Newtonsoft.Json.Linq.JObject
                {
                    { "id", taskModel.Id.ToString() },
                    { "Title", taskTitle },
                    { "FlowName", taskModel.FlowName },
                    { "StepName", taskModel.StepName },
                    { "ReceiveName", taskModel.ReceiveName },
                    { "ReceiveTime", taskModel.ReceiveTime.ToDateTimeString() },
                    { "StatusTitle", status },
                    { "Opation", opation }
                };
                jArray.Add(jObject);
            }
            return "{\"userdata\":{\"total\":" + count + ",\"pagesize\":" + size + ",\"pagenumber\":" + number + "},\"rows\":" + jArray.ToString() + "}";
        }

        /// <summary>
        /// 删除实例
        /// </summary>
        /// <returns></returns>
        [Validate]
        public string DeleteInstance()
        {
            string groupId = Request.Querys("groupid");
            if (!groupId.IsGuid(out Guid guid))
            {
                return "组ID错误!";
            }
            Business.FlowTask flowTask = new Business.FlowTask();
            var groupTasks = flowTask.GetListByGroupId(guid);
            if (groupTasks.Count > 0)
            {
                flowTask.DeleteByGroupId(groupTasks.ToArray());
                
                Business.Log.Add("删除了流程实例-" + groupTasks.First().Title, Newtonsoft.Json.JsonConvert.SerializeObject(groupTasks), Business.Log.Type.流程运行);
            }
            return "删除成功!";
        }

        /// <summary>
        /// 实例管理
        /// </summary>
        /// <returns></returns>
        [Validate]
        public IActionResult InstanceManage()
        {
            string groupId = Request.Querys("groupid");
            Business.FlowTask flowTask = new Business.FlowTask();
            var groupTasks = flowTask.GetListByGroupId(groupId.ToGuid());
            Newtonsoft.Json.Linq.JArray jArray = new Newtonsoft.Json.Linq.JArray();
            foreach (var groupTask in groupTasks)
            {
                Newtonsoft.Json.Linq.JObject jObject = new Newtonsoft.Json.Linq.JObject();
                string opation = "<a class=\"list\" href=\"javascript:void(0);\" onclick=\"cngStatus('" + groupTask.Id.ToString() + "');\"><i class=\"fa fa-exclamation\"></i>状态</a>";
                if (groupTask.ExecuteType.In(0, 1) && 5 != groupTask.TaskType)
                {
                    opation += "<a class=\"list\" href=\"javascript:void(0);\" onclick=\"designate('" + groupTask.Id.ToString() + "');\"><i class=\"fa fa-hand-o-right\"></i>指派</a>"
                            + "<a class=\"list\" href=\"javascript:void(0);\" onclick=\"goTo('" + groupTask.Id.ToString() + "');\"><i class=\"fa fa-level-up\"></i>跳转</a>";
                }
                jObject.Add("id", groupTask.Id);
                jObject.Add("StepID", groupTask.StepName);
                jObject.Add("SenderName", groupTask.SenderName);
                jObject.Add("ReceiveTime", groupTask.ReceiveTime.ToDateTimeString());
                jObject.Add("ReceiveName", groupTask.ReceiveName);
                jObject.Add("CompletedTime1", groupTask.CompletedTime1.HasValue ? groupTask.CompletedTime1.Value.ToDateTimeString() : "");
                jObject.Add("Status", flowTask.GetExecuteTypeTitle(groupTask.ExecuteType));
                jObject.Add("Comment", groupTask.Comments);
                jObject.Add("Note", groupTask.Note);
                jObject.Add("Opation", opation);
                jArray.Add(jObject);
            }
            ViewData["json"] = jArray.ToString();
            ViewData["appid"] = Request.Querys("appid");
            ViewData["iframeid"] = Request.Querys("iframeid");
            return View();
        }

        /// <summary>
        /// 改变任务状态
        /// </summary>
        /// <returns></returns>
        [Validate]
        public IActionResult ChangeStatus()
        {
            string taskId = Request.Querys("taskid");
            Business.FlowTask flowTask = new Business.FlowTask();
            var taskModel = flowTask.Get(taskId.ToGuid());
            if (null == taskModel)
            {
                return new ContentResult() { Content = "未找到当前任务!" };
            }
            ViewData["queryString"] = Request.UrlQuery();
            ViewData["statusOptions"] = new Business.FlowTask().GetExecuteTypeOptions(taskModel.ExecuteType);
            return View();
        }

        /// <summary>
        /// 保存任务状态
        /// </summary>
        /// <returns></returns>
        [Validate]
        [ValidateAntiForgeryToken]
        public string SaveStatus()
        {
            string status = Request.Forms("status");
            string taskId = Request.Querys("taskid");
            if (!taskId.IsGuid(out Guid guid))
            {
                return "任务ID错误!";
            }
            if (!status.IsInt(out int i))
            {
                return "状态错误!";
            }
            Business.FlowTask flowTask = new Business.FlowTask();
            var taskModel = flowTask.Get(guid);
            string oldModel = taskModel.ToString();
            if (null == taskModel)
            {
                return "未找到当前任务!";
            }
            taskModel.ExecuteType = i;
            if (i < 2)
            {
                taskModel.Status = i;
            }
            else
            {
                taskModel.Status = 2;
            }
            flowTask.Update(taskModel);
            Business.Log.Add("改变了任务状态-" + taskModel.Title, "", Business.Log.Type.流程运行, oldModel, taskModel.ToString());
            return "保存成功!";
        }

        /// <summary>
        /// 指派任务
        /// </summary>
        /// <returns></returns>
        [Validate]
        public IActionResult Designate()
        {
            ViewData["queryString"] = Request.UrlQuery();
            return View();
        }

        /// <summary>
        /// 保存指派任务
        /// </summary>
        /// <returns></returns>
        [Validate]
        [ValidateAntiForgeryToken]
        public string SaveDesignate()
        {
            string taskid = Request.Querys("taskid");
            string user = Request.Forms("user");
            if (!taskid.IsGuid(out Guid taskId))
            {
                return "任务ID错误!";
            }
            if (user.IsNullOrWhiteSpace())
            {
                return "没有选择要指派的人员!";
            }
            Business.FlowTask flowTask = new Business.FlowTask();
            var taskModel = flowTask.Get(taskId);
            if (null == taskModel)
            {
                return "没有找到当前任务!";
            }
            string msg = flowTask.Designate(taskModel, new Business.Organize().GetAllUsers(user));
            Business.Log.Add("指派了任务-" + taskModel.Title, taskModel.ToString() + "-" + msg, Business.Log.Type.流程运行, others: user);
            return "1".Equals(msg) ? "指派成功!" : msg;
        }

        /// <summary>
        /// 跳转
        /// </summary>
        /// <returns></returns>
        [Validate]
        public IActionResult GoTo()
        {
            string taskid = Request.Querys("taskid");
            if (!taskid.IsGuid(out Guid taskId))
            {
                return new ContentResult() { Content= "任务ID错误!" };
            }
            var taskModel = new Business.FlowTask().Get(taskId);
            if (null == taskModel)
            {
                return new ContentResult() { Content = "未找到当前任务!" };
            }
            var flowRunModel = new Business.Flow().GetFlowRunModel(taskModel.FlowId);
            if (null == flowRunModel)
            {
                return new ContentResult() { Content = "未找到当前流程运行时!" };
            }
            ViewData["steps"] = flowRunModel.Steps;
            ViewData["queryString"] = Request.UrlQuery();
            return View();
        }

        /// <summary>
        /// 保存跳转
        /// </summary>
        /// <returns></returns>
        [Validate]
        [ValidateAntiForgeryToken]
        public string SaveGoTo()
        {
            string taskid = Request.Querys("taskid");
            string steps = Request.Forms("step");
            if (!taskid.IsGuid(out Guid taskId))
            {
                return "任务ID错误!";
            }
            Business.FlowTask flowTask = new Business.FlowTask();
            var taskModel = flowTask.Get(taskId);
            if (null == taskModel)
            {
                return "未找到当前任务!";
            }
            Dictionary<Guid, List<Model.User>> dicts = new Dictionary<Guid, List<Model.User>>();
            Business.Organize organize = new Business.Organize();
            foreach (string step in steps.Split(","))
            {
                if (!step.IsGuid(out Guid stepId))
                {
                    continue;
                }
                string member = Request.Forms("member_" + step);
                if (member.IsNullOrWhiteSpace())
                {
                    continue;
                }
                dicts.Add(stepId, organize.GetAllUsers(member));
            }
            string msg = new Business.FlowTask().GoTo(taskModel, dicts);
            Business.Log.Add("跳转了任务-" + taskModel.Title, taskModel.ToString() + "-" + msg, Business.Log.Type.流程运行, others: Newtonsoft.Json.JsonConvert.SerializeObject(dicts));
            return "1".Equals(msg) ? "跳转成功!" : msg;
        }

        /// <summary>
        /// 查看处理过程
        /// </summary>
        /// <returns></returns>
        [Validate(CheckApp = false)]
        public IActionResult Detail()
        {
            string flowid = Request.Querys("flowid");
            string groupid = Request.Querys("groupid");
            string taskid = Request.Querys("taskie");
            string appid = Request.Querys("appid");
            string tabid = Request.Querys("tabid");
            string displaymodel = Request.Querys("displaymodel");

            Business.FlowTask bflowTask = new Business.FlowTask();
            var flowTasks = bflowTask.GetListByGroupId(groupid.ToGuid());
            Newtonsoft.Json.Linq.JArray jArray = new Newtonsoft.Json.Linq.JArray();
            Newtonsoft.Json.Linq.JArray jArray1 = new Newtonsoft.Json.Linq.JArray();
            foreach (var flowTask in flowTasks)
            {
                Newtonsoft.Json.Linq.JObject jObject = new Newtonsoft.Json.Linq.JObject
                {
                    { "StepName", flowTask.StepName },
                    { "SenderName", flowTask.SenderName },
                    { "SenderTime", flowTask.ReceiveTime.ToDateTimeString() },
                    { "ReceiveName", flowTask.ReceiveName },
                    { "CompletedTime1", flowTask.CompletedTime1.ToDateTimeString() },
                    { "StatusTitle", bflowTask.GetExecuteTypeTitle(flowTask.ExecuteType) },
                    { "Comment", flowTask.Comments },
                    { "Note", flowTask.Note }
                };
                Newtonsoft.Json.Linq.JObject jObject1 = new Newtonsoft.Json.Linq.JObject
                {
                    { "stepid", flowTask.StepId },
                    { "prevstepid", flowTask.PrevStepId },
                    { "status", flowTask.Status },
                    { "sender", flowTask.SenderName },
                    { "sendtime", flowTask.ReceiveTime.ToDateTimeString() },
                    { "receiver", flowTask.ReceiveName },
                    { "statustitle", "" }
                };
                jArray.Add(jObject);
                jArray1.Add(jObject1);
            }
            ViewData["json"] = jArray.ToString();
            ViewData["json1"] = jArray1.ToString();
            ViewData["displaymodel"] = displaymodel.IsNullOrWhiteSpace() ? "0" : displaymodel;
            ViewData["tabid"] = tabid;
            ViewData["query"] = "flowid=" + flowid +"&stepid=" + Request.Querys("stepid") + "&groupid=" + groupid + "&taskid=" + taskid + "&appid=" + appid + "&tabid=" + tabid + "&iframeid=" + Request.Querys("iframeid");
            ViewData["flowid"] = flowid;
            return View();
        }

        /// <summary>
        /// 查看子流程处理过程
        /// </summary>
        /// <returns></returns>
        [Validate(CheckApp = false)]
        public IActionResult DetailSubFlow()
        {
            string taskid = Request.Querys("taskid");
            if (!taskid.IsGuid(out Guid taskId))
            {
                return new ContentResult() { Content = "任务ID错误!" };
            }
            Business.FlowTask flowTask = new Business.FlowTask();
            var currentTask = flowTask.Get(taskId);
            if (null == currentTask)
            {
                return new ContentResult() { Content = "未找到当前任务!" };
            }
            if (currentTask.SubFlowGroupId.IsNullOrWhiteSpace())
            {
                return new ContentResult() { Content = "未找到当前任务的子流程任务!" };
            }
            string[] subFlowGroupIds = currentTask.SubFlowGroupId.Split(',');

            if (subFlowGroupIds.Length == 1)
            {
                var groupTasks = flowTask.GetListByGroupId(subFlowGroupIds[0].ToGuid());
                if (groupTasks.Count > 0)
                {
                    return Redirect("Detail?flowid=" + groupTasks.First().FlowId + "&stepid=" + groupTasks.First().StepId +
                        "&groupid=" + subFlowGroupIds[0] + "&taskid=" + groupTasks.First().Id + "&appid=" + Request.Querys("appid")
                        + "&tabid=" + Request.Querys("tabid") + "&iframeid=" + Request.Querys("iframeid"));
                }
            }
            else
            {
                Newtonsoft.Json.Linq.JArray jArray = new Newtonsoft.Json.Linq.JArray();
                foreach (var id in subFlowGroupIds)
                {
                    var groupTasks = flowTask.GetListByGroupId(id.ToGuid());
                    if (groupTasks.Count == 0)
                    {
                        continue;
                    }
                    var groupTask = groupTasks.First();
                    Newtonsoft.Json.Linq.JObject jObject = new Newtonsoft.Json.Linq.JObject
                    {
                        { "FlowName", groupTask.FlowName },
                        { "StepName", groupTask.StepName },
                        { "SenderName", groupTask.SenderName },
                        { "SenderTime", groupTask.ReceiveTime.ToDateTimeString() },
                        { "ReceiveName", groupTask.ReceiveName },
                        { "CompletedTime1", groupTask.CompletedTime1.HasValue ? groupTask.CompletedTime1.Value.ToShortDateString() : "" },
                        { "StatusTitle", flowTask.GetExecuteTypeTitle(groupTask.ExecuteType) },
                        { "Show", "<a class=\"list\" href=\"javascript:void(0);\" onclick=\"detail('" + groupTask.FlowId + "', '" + groupTask.GroupId + "');return false;\"><i class=\"fa fa-search\"></i>查看</a>" }
                    };
                    jArray.Add(jObject);
                }
                ViewData["json"] = jArray.ToString();
            }
            return View();
        }


    }
}