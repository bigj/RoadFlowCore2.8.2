﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RoadFlow.Utility;

namespace RoadFlow.IntegrateCore.Areas.RoadFlowCore.Controllers
{
    [Area("RoadFlowCore")]
    public class LogController : Controller
    {
        [Validate]
        public IActionResult Index()
        {
            string appId = Request.Querys("appid");
            string tabId = Request.Querys("tabid");
            ViewData["appId"] = appId;
            ViewData["tabId"] = tabId;
            ViewData["typeOptions"] = new Business.Log().GetTypeOptions();
            return View();
        }

        [Validate]
        public string Query()
        {
            string sidx = Request.Forms("sidx");
            string sord = Request.Forms("sord");
            string Title = Request.Forms("Title");
            string Type = Request.Forms("Type");
            string UserID = Request.Forms("UserID");
            string Date1 = Request.Forms("Date1");
            string Date2 = Request.Forms("Date2");

            int size = Tools.GetPageSize();
            int number = Tools.GetPageNumber();
            string order = (sidx.IsNullOrEmpty() ? "WriteTime" : sidx) + " " + (sord.IsNullOrEmpty() ? "DESC" : sord);


            var logs = new Business.Log().GetPagerList(out int count, size, number, Title, Type, UserID.RemoveUserPrefix(), Date1, Date2, order);
            Newtonsoft.Json.Linq.JArray jArray = new Newtonsoft.Json.Linq.JArray();
            foreach (System.Data.DataRow dr in logs.Rows)
            {
                Newtonsoft.Json.Linq.JObject jObject = new Newtonsoft.Json.Linq.JObject
                {
                    { "id", dr["Id"].ToString() },
                    { "Title", "<a href='javascript:void(0);' class='blue' onclick=\"detail('" + dr["Id"].ToString() + "');\">" + dr["Title"].ToString() + "</a>" },
                    { "Type", dr["Type"].ToString() },
                    { "WriteTime", dr["WriteTime"].ToString().ToDateTime().ToDateTimeString() },
                    { "UserName", dr["UserName"].ToString() },
                    { "IPAddress", dr["IPAddress"].ToString() },
                    { "Opation", "<a href='javascript:void(0);' class='list' onclick=\"detail('" + dr["Id"].ToString() + "');\"><i class='fa fa-search'></i>查看</a>" }
                };
                jArray.Add(jObject);
            }
            return "{\"userdata\":{\"total\":" + count + ",\"pagesize\":" + size + ",\"pagenumber\":" + number + "},\"rows\":" + jArray.ToString() + "}";
        }

        [Validate]
        public IActionResult Detail()
        {
            string logId = Request.Querys("logid");
            var logModel = new Business.Log().Get(logId.ToGuid());
            return View(logModel);
        }
    }
}