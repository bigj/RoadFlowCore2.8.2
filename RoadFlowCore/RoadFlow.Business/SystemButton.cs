﻿using System;
using System.Collections.Generic;
using System.Text;
using RoadFlow.Utility;
using System.Linq;

namespace RoadFlow.Business
{
    public class SystemButton
    {
        private readonly Data.SystemButton systemButtonData;
        public SystemButton()
        {
            systemButtonData = new Data.SystemButton();
        }

        /// <summary>
        /// 得到所有按钮
        /// </summary>
        /// <returns></returns>
        public List<Model.SystemButton> GetAll()
        {
            return systemButtonData.GetAll();
        }
        /// <summary>
        /// 查询一个按钮
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Model.SystemButton Get(Guid id)
        {
            return systemButtonData.Get(id);
        }
        /// <summary>
        /// 添加一个按钮
        /// </summary>
        /// <param name="systemButton">字典实体</param>
        /// <returns></returns>
        public int Add(Model.SystemButton systemButton)
        {
            return systemButtonData.Add(systemButton);
        }
        /// <summary>
        /// 更新按钮
        /// </summary>
        /// <param name="systemButton">字典实体</param>
        public int Update(Model.SystemButton systemButton)
        {
            return systemButtonData.Update(systemButton);
        }
        /// <summary>
        /// 删除一个按钮
        /// </summary>
        /// <param name="systemButton">字典实体</param>
        /// <returns></returns>
        public int Delete(Model.SystemButton systemButton)
        {
            return systemButtonData.Delete(systemButton);
        }

        /// <summary>
        /// 删除一批按钮
        /// </summary>
        /// <param name="ids">id字符串，多个逗号分开</param>
        /// <returns></returns>
        public List<Model.SystemButton> Delete(string ids)
        {
            List<Model.SystemButton> systemButtons = new List<Model.SystemButton>();
            foreach (string id in ids.Split(','))
            {
                var but = Get(id.ToGuid());
                if (null != but)
                {
                    systemButtons.Add(but);
                }
            }
            systemButtonData.Delete(systemButtons.ToArray());
            return systemButtons;
        }
        /// <summary>
        /// 得到最大排序
        /// </summary>
        /// <returns></returns>
        public int GetMaxSort()
        {
            var allButtons = GetAll();
            return allButtons.Count == 0 ? 5 : allButtons.Max(p => p.Sort) + 5;
        }
        /// <summary>
        /// 得到所有按钮下拉选项
        /// </summary>
        /// <param name="value">默认值</param>
        /// <returns></returns>
        public string GetOptions(string value = "")
        {
            var buttons = GetAll();
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var button in buttons)
            {
                stringBuilder.Append("<option value=\"" + button.Id + "\"" + (button.Id.ToString().EqualsIgnoreCase(value) ? " selected=\"selected\"" : "") + ">" + button.Name + "</option>");
            }
            return stringBuilder.ToString();
        }
        /// <summary>
        /// 得到所有按钮JSON
        /// </summary>
        /// <returns></returns>
        public Newtonsoft.Json.Linq.JArray GetAllJson()
        {
            var buttons = GetAll();
            Newtonsoft.Json.Linq.JArray jArray = new Newtonsoft.Json.Linq.JArray();
            foreach (var button in buttons)
            {
                jArray.Add(Newtonsoft.Json.Linq.JObject.Parse(button.ToString()));
            }
            return jArray;
        }
        /// <summary>
        /// 得到按钮类型下拉选择
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public string GetButtonTypeOptions(string value = "")
        {
           return "<option value=\"0\"" + ("0".EqualsIgnoreCase(value) ? " selected=\"selected\"" : "") + ">普通按钮</option>"
                + "<option value=\"1\"" + ("1".EqualsIgnoreCase(value) ? " selected=\"selected\"" : "") + ">列表按钮</option>"
                + "<option value=\"2\"" + ("2".EqualsIgnoreCase(value) ? " selected=\"selected\"" : "") + ">工具栏按钮</option>";
        }
    }
}
