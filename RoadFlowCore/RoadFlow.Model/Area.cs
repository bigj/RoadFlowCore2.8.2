﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RoadFlow.Model
{
    public class Area
    {



        [Key]
        public Guid Id { get; set; }



        [DisplayName("名称")]
        public string Name { get; set; }
    }
}
